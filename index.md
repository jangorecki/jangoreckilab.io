
# navigate content

 - [blog](./blog)
 - [talks](./r-talks)
 - [courses](./r-courses)
 - [about](./about)

# R repositories

- [`data.table`](./data.table/library/data.table/html/00Index.html)
- [`data.cube`](./data.cube/library/data.cube/html/00Index.html)
- [`big.data.table`](./big.data.table/library/big.data.table/html/00Index.html)
